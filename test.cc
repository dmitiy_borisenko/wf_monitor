#include <cstdio>

struct TestStruct
{
    int* i_ptr;
    double d;

    TestStruct() : i_ptr(), d() {}
};

int main()
{
    TestStruct arr[10];
    for (int i=0; i<sizeof(arr)/sizeof(TestStruct); ++i)
        printf("%p\t%f\n", arr[i].i_ptr, arr[i].d);
    return 0;
}