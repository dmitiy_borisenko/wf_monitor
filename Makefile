BLD_DIR := bld

# Dictionaries
ENTPHOTS_DICT := $(BLD_DIR)/EntPhotsDict.cxx

# Libraries 
ENTPHOTS_LIB := $(BLD_DIR)/libEntPhots.so

# Exes
VIEWER_EXE := $(BLD_DIR)/viewer

CLING = rootcling -I$(realpath inc) -f $@ -c $(foreach header,$^,$(notdir $(header)))
COMPILER := g++ -g $(shell root-config --cflags --glibs) -lSpectrum -I./inc
LIB_COMPILER = $(COMPILER) -shared -fPIC -o $@ $^
EXE_COMPILER = $(COMPILER) -L$(BLD_DIR) -lEntPhots -o $@ $<

.PHONY: clean 

$(VIEWER_EXE): main.cc $(ENTPHOTS_LIB)
	$(EXE_COMPILER) 

$(ENTPHOTS_LIB): src/Adc64Rdr.cc src/Viewer.cc src/ArmView.cc src/ArmLayout.cc $(ENTPHOTS_DICT)
	$(LIB_COMPILER)

$(ENTPHOTS_DICT):: inc/Adc64Rdr.h inc/Viewer.h inc/ArmView.h inc/ArmLayout.h LinkDef.h
	$(CLING)

clean:
	@find $(BLD_DIR)/* -exec rm -vf {} \;