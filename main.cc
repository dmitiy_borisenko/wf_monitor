#include "Viewer.h"
#include <TApplication.h>

int main(int argc, char** argv)
{
    TApplication app("Viewer", &argc, argv);
    Viewer v("..");
    app.Run();
    return 0; 
}