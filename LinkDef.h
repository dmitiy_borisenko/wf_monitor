#ifdef ADC64_RDR_H
    #pragma link C++ class Adc64Rdr;
#endif

#ifdef VIEWER_H
    #pragma link C++ class Viewer;
#endif

#ifdef ARM_VIEW_H
    #pragma link C++ class ArmView;
#endif

#ifdef ARM_LAYOUT_H
    #pragma link C++ class ArmLayout;
#endif