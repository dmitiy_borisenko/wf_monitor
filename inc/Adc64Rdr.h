#ifndef ADC64_RDR_H
#define ADC64_RDR_H

#include <TGraph.h>
#include <TH1.h>


#define PAR(PAR_NAME) Adc64Rdr::get_ ## PAR_NAME () 


const int MAX_N_SAMPLES = 2048;
const int ARM_SIZE = 17;


enum FrameType { NaI_FRAME, SCAT_FRAME };
enum AssetType { WF, AMP, BEG };
enum ExtraActions { NOTHING = 0, REDRAW = 0b01, PRINT = 0b10 };

struct Frame 
{
    Short_t wf_size;
    Short_t wf[MAX_N_SAMPLES];
    
    TGraph wf_repr;
    FrameType type;

    // Frame(FrameType);
};

struct WfParams 
{

    Int_t amp;
    Short_t beg;
    Short_t end;

    TH1F hAmp;
    TH1F hBeg;

    void initHists(TString orient, int idx, FrameType frmType);
};


class TTree;

class Adc64Rdr : public TObject
{
public:
    Adc64Rdr(TString data_loc);
    void nextEvt();
    void prevEvt();
    void drawLeftAsset(AssetType t, int ch);
    void drawRightAsset(AssetType t, int ch);
    void createParamsTree();
    void fitLeftAsset(AssetType t, int ch);
    void fitRightAsset(AssetType t, int ch);
    void saveAmpHists();

    static void setZLvlEvalParams(int n_beg_samples, int n_beg_subranges);
    static void setSigEvalParams(int thres, int sig_len_NaI, int sig_len_scat);
    static void setAmpHistParams(int nbins, int amp_max_NaI, int amp_max_scat);
    static void setBegHistParams(int nbins_NaI, int nbins_Scat);
    static void setFitParams(int bg_iters, int sigma_nbins, double nsigmas);

    static const int&       get_N_BEG_SAMPLES()         { return N_BEG_SAMPLES; }
    static const int&       get_N_BEG_SUBRANGES()       { return N_BEG_SUBRANGES; }    
    static const int&       get_SUBRANGE_SIZE()         { return SUBRANGE_SIZE; }
    static const int&       get_THRES()                 { return THRES; }
    static const int*       get_SIG_LEN()               { return SIG_LEN; }
    static const int&       get_HIST_AMP_NBINS()        { return HIST_AMP_NBINS; }
    static const int*       get_HIST_AMP_MAX()          { return HIST_AMP_MAX; }
    static const int*       get_HIST_BEG_NBINS()        { return HIST_BEG_NBINS; }
    static const int&       get_GFIT_BG_ITERS()         { return GFIT_BG_ITERS; }
    static const int&       get_GFIT_INIT_SIGMA_NBINS() { return GFIT_INIT_SIGMA_NBINS; }
    static const double&    get_GFIT_NSIGMAS()          { return GFIT_NSIGMAS; }         

private:
    // fit params
    static int N_BEG_SAMPLES;
    static int N_BEG_SUBRANGES;
    static int SUBRANGE_SIZE;
    static int THRES;
    static int SIG_LEN[];
    // hists params
    static int HIST_AMP_NBINS;
    static int HIST_AMP_MAX[];
    static int HIST_BEG_NBINS[];
    // gaus fit params
    static int GFIT_BG_ITERS = 20;
    static int GFIT_INIT_SIGMA_NBINS = 5;
    static double GFIT_NSIGMAS = 3;    
    // dummy var
    static bool inited;

    // assets for reading waveforms
    TTree* adc64_data;
    Frame lArm[ARM_SIZE];
    Frame rArm[ARM_SIZE];
    Long64_t curEvt;
    // assets for writing params
    TTree* wf_params;
    WfParams lArmParams[ARM_SIZE];
    WfParams rArmParams[ARM_SIZE];

    static bool init_stat();

    void init(TString fname);
    void connectBranch(const char*, Frame*);
    void loadCurEntry();
    void fillWfRepr(Frame &);
    void fillWfReprs();

ClassDef(Adc64Rdr, 0)
};

#endif