#ifndef VIEWER_H
#define VIEWER_H

#include <TGFrame.h>

class Adc64Rdr;
class ArmView;

class Viewer : public TGMainFrame {

public:
    Viewer(const char* data_loc);
    ~Viewer();
    void prevEvt();
    void nextEvt();
    void extractParams();

private:
    Adc64Rdr* rdr;
    // wf monitor part
    ArmView* lView;
    ArmView* rView;
    TGCompositeFrame* tab1;
    // wf parametrization part
    TGCompositeFrame* tab2;
    TGCompositeFrame* histCont;
}; 

#endif