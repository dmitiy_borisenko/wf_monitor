#ifndef ARM_VIEW_H
#define ARM_VIEW_H

#include <TGFrame.h>
#include <vector>

class TRootEmbeddedCanvas;

class ArmView : public TGGroupFrame
{
public:
    ArmView(int armSize, const TGWindow* p=0);
    void cd(int d);
    void update();

protected:
    virtual void DoRedraw();

private:
    std::vector<TRootEmbeddedCanvas*> dets;
};

#endif