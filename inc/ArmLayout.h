#ifndef ARM_LAYOUT_H
#define ARM_LAYOUT_H

#include <TGLayout.h>

class ArmLayout : public TGLayoutManager
{
public:
    ArmLayout(TGCompositeFrame* main);
    void Layout();
    TGDimension GetDefaultSize() const;

private:
    TGCompositeFrame* fMain;
};

#endif