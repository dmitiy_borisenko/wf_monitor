#include "ArmView.h"
// #include <TGLayout.h>
// #include <TGButton.h>
#include "ArmLayout.h"
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
// #include <TMath.h>
// #include <TVirtualX.h>

// Квадратный фрейм, содержащий:
// - по периметру и центре TRootEmbeddedCanvas'ы для отображения вейвформ
// - в промежуточном положении схему расположения NaI-детекторов


ArmView::ArmView(int armSize, const TGWindow* p)
    : TGGroupFrame(p)
{
    SetLayoutManager( new ArmLayout(this) );
    // SetLayoutManager( new TGHorizontalLayout(this) );

    for (int i=0; i < armSize; ++i)
    {
        dets.push_back( new TRootEmbeddedCanvas("", this) );
        AddFrame(dets.back());
    }

    // AddFrame(new TGTextButton(this, "Btn#1"), new TGLayoutHints(kLHintsLeft));
    // AddFrame(new TGTextButton(this, "Btn#2"), new TGLayoutHints(kLHintsRight));
}

void ArmView::DoRedraw()
{
    TGGroupFrame::DoRedraw();
}

void ArmView::cd(int d)
{
    dets[d]->GetCanvas()->cd();
}

void ArmView::update()
{
    for (TRootEmbeddedCanvas* det : dets)
    {
        det->GetCanvas()->Modified();   
        det->GetCanvas()->Update();   
    }
}