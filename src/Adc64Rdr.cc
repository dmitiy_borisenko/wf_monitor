#include "Adc64Rdr.h"
#include <TFile.h>
#include <TTree.h>
#include <TList.h>
#include <TSystemDirectory.h>
#include <TROOT.h>
#include <TSystem.h>
#include <numeric>
#include <TMath.h>
#include <TSpectrum.h>
#include <TF1.h>

typedef std::pair<int,int> Zone;
typedef std::vector<Zone> ZoneV;

const char* WF_PARAMS_SPEC = "amp/I:beg/S:end/S";


struct FitResult
{
    TH1* hists[3];
    double mean;
    double sigma;

    FitResult();
    FitResult(const double* fit_res);
    FitResult(TH1* smoothed, TH1* bg, TH1* fit, const double* fit_res);
    void draw();
};
FitResult::FitResult() 
    : hists{nullptr, nullptr, nullptr}
    , mean()
    , sigma() 
{}
FitResult::FitResult(const double* fit_res)
    : hists{nullptr, nullptr, nullptr}
    , mean(fit_res[0])
    , sigma(fit_res[1])
{}
FitResult::FitResult(TH1* smoothed, TH1* bg, TH1* fit, const double* fit_res) 
    : hists{smoothed, bg, fit}
    , mean(fit_res[0])
    , sigma(fit_res[1])
{}
void FitResult::draw() 
{ 
    for (int i=0; i<sizeof(hists)/sizeof(TH1*); ++i) hists[i]->Draw("same"); 
}



void calcParams(const Frame &frm, WfParams &prm)
{
    prm.amp = prm.beg = prm.end = -1;
    if (frm.wf_size == 0) return;

    // calculate several values of <local zero_lvl> and <local std_dev from zero_lvl>
    double zero_lvls[PAR(N_BEG_SUBRANGES)], std_devs[PAR(N_BEG_SUBRANGES)];
    int i;
    for (i=0; i < PAR(N_BEG_SUBRANGES); ++i)
    {
        if ( (i + 1) * PAR(SUBRANGE_SIZE) > frm.wf_size ) break;
        
        zero_lvls[i] = std_devs[i] = 0;
        for (int s = i * PAR(SUBRANGE_SIZE); s < (i+1) * PAR(SUBRANGE_SIZE); ++s)
        {
            zero_lvls[i] += frm.wf[s];
            std_devs[i] += frm.wf[s] * frm.wf[s];
        }

        zero_lvls[i] /= PAR(SUBRANGE_SIZE);
        std_devs[i] = sqrt(std_devs[i] / PAR(SUBRANGE_SIZE) - zero_lvls[i] * zero_lvls[i]);
    }
    
    // choose final zero_lvl value
    double std_dev = std::numeric_limits<double>::max();
    double zero_lvl;
    for (int j=0; j < i; ++j)
    {
        if (std_devs[j] < std_dev) 
        {
            std_dev = std_devs[j];
            zero_lvl = zero_lvls[j];
        }
    }

    // evaluate wf's <amp> and [beg, end]
    double val;
    prm.amp = 1;
    for (int s=PAR(N_BEG_SAMPLES); s < frm.wf_size; ++s)
    {
        if ( frm.wf[s] - zero_lvl < PAR(THRES) )
        {
            prm.beg = s;
            prm.end = (Short_t)std::min((int)frm.wf_size, s + PAR(SIG_LEN)[frm.type]);
            
            for (int s2=s; s2 < prm.end; ++s2)
            {
                val = frm.wf[s2] - zero_lvl; 
                if ( val < prm.amp ) prm.amp = val; 
            }
        
            break;
        }
    }
    prm.amp = std::abs(prm.amp);

    // fill histograms
    if ( prm.beg > -1 && frm.type == NaI_FRAME )
    {
        prm.hAmp.Fill(prm.amp);
        prm.hBeg.Fill(prm.beg);
    }
}


std::pair<double, double> fitHist(TH1* h)
{
    TH1 *smoothed, *bg, *woBg, *fit;
    pair<double, double> peak, fit_range;
    
    // smoothed
    smoothed = (TH1*)orig->Clone("smoothed");
    smoothed->Smooth();
    smoothed->SetLineColor(kYellow);

    // bg, woBg
    TString orig_name(orig->GetName());
    orig_name.ToLower();
    Option_t* opt = Form("nosmoothing%s", orig_name.Contains("sct") ? ";compton" : "");
    bg = TSpectrum::StaticBackground(smoothed, N_ITERS, opt);
    bg->SetLineColor(kGreen);
    woBg = (TH1*) smoothed->Clone("woBg");
    woBg->Add(bg, -1);

    // peak search
    int n_peaks = sp.Search(woBg, 2, "nomakrov;nosmoothing;nodraw");
    if (n_peaks != 1) 
    {
        printf("ERROR! n_peaks!=1\n");
        return FitResult();
    }
    peak = make_pair(sp.GetPositionX()[0], sp.GetPositionY()[0]);

    // gaus fit_0
    double init_sigma = orig->GetBinWidth(1) * GFIT_INIT_SIGMA_NBINS;
    fit_range = make_pair(peak.first - init_sigma, peak.first + init_sigma);
    printf("\tinit fit_range: [%.0f, %.0f]\n", fit_range.first, fit_range.second);
    TF1 fit_0("fit_0", "gaus", fit_range.first, fit_range.second);
    fit_0.SetParameters(peak.second, peak.first);
    woBg->Fit(&fit_0, "N0Q", "", fit_range.first, fit_range.second);
    
    // gaus fit_1
    double half_width = fit_0.GetParameter(2) * GFIT_NSIGMAS;
    fit_range = make_pair(peak.first - half_width, peak.first + half_width);
    printf("\tfit_range: [%.0f, %.0f]\n", fit_range.first, fit_range.second);
    TF1 fit_1("fit_1", "gaus", fit_range.first, fit_range.second);
    woBg->Fit(&fit_1, "N0Q", "", fit_range.first, fit_range.second);
    delete woBg;

    // return result
    fit = (TH1*)bg->Clone("fit");
    fit->Add(&fit_1);
    fit->SetLineColor(kRed);
    return FitResult(smoothed, bg, fit, 
}


/*
Frame::Frame(FrameType t)
    : type(t)
{}
*/

void WfParams::initHists(TString orient, int idx, FrameType frmType)
{
    TString name, title; 
    if ( !orient.CompareTo("L", TString::kIgnoreCase) )
    {
        name += "l_";
        title += "Left ";
    }
    else if ( !orient.CompareTo("R", TString::kIgnoreCase) )
    {
        name += "r_";
        title += "Right ";
    }
    switch (frmType)
    {
        case NaI_FRAME:
            name += Form("NaI%d", idx);
            title += Form("NaI#%d", idx);
        break;
        case SCAT_FRAME:
            name += "Sct";
            title += "Scatterer";
    }

    hAmp.SetName(name);
    hAmp.SetTitle(title);
    hAmp.GetXaxis()->Set(PAR(HIST_AMP_NBINS), 0, PAR(HIST_AMP_MAX)[frmType]);
    hAmp.Rebuild();

    hBeg.SetName(name);
    hBeg.SetTitle(title);
    hBeg.GetXaxis()->Set(PAR(HIST_BEG_NBINS)[frmType], 0, PAR(HIST_BEG_NBINS)[frmType]);
    hBeg.Rebuild();
}


int Adc64Rdr::N_BEG_SAMPLES;
int Adc64Rdr::N_BEG_SUBRANGES;
int Adc64Rdr::SUBRANGE_SIZE;
int Adc64Rdr::THRES;
int Adc64Rdr::SIG_LEN[2];
int Adc64Rdr::HIST_AMP_NBINS;
int Adc64Rdr::HIST_AMP_MAX[2];
int Adc64Rdr::HIST_BEG_NBINS[2];
int Adc64Rdr::FIT_BG_ITERS;

void Adc64Rdr::setZLvlEvalParams(int n_beg_samples, int n_beg_subranges)
{
    N_BEG_SAMPLES = n_beg_samples;
    N_BEG_SUBRANGES = n_beg_subranges;
    SUBRANGE_SIZE = N_BEG_SAMPLES / N_BEG_SUBRANGES;
}

void Adc64Rdr::setSigEvalParams(int thres, int sig_len_NaI, int sig_len_scat)
{
    THRES = thres;
    SIG_LEN[NaI_FRAME] = sig_len_NaI;
    SIG_LEN[SCAT_FRAME] = sig_len_scat;
}

void Adc64Rdr::setAmpHistParams(int nbins, int amp_max_NaI, int amp_max_scat)
{
    HIST_AMP_NBINS = nbins;
    HIST_AMP_MAX[NaI_FRAME] = amp_max_NaI;
    HIST_AMP_MAX[SCAT_FRAME] = amp_max_scat;
}

void Adc64Rdr::setBegHistParams(int nbins_NaI, int nbins_Scat)
{
    HIST_BEG_NBINS[NaI_FRAME] = nbins_NaI;
    HIST_BEG_NBINS[SCAT_FRAME] = nbins_Scat;
}

void Adc64Rdr::setFitParams(int bg_iters, int sigma_nbins, double nsigmas)
{
    GFIT_BG_ITERS = bg_iters;
    GFIT_INIT_SIGMA_NBINS = sigma_nbins;
    GFIT_NSIGMAS = nsigmas
}

bool Adc64Rdr::init_stat()
{
    setZLvlEvalParams(48, 3);
    setSigEvalParams(-1000, 50, 5);
    setAmpHistParams(100, 10000, 40000);
    setBegHistParams(350, 71);
    setFitParams(20);

    return true;
}

bool Adc64Rdr::inited = Adc64Rdr::init_stat();


Adc64Rdr::Adc64Rdr(TString data_loc)
    : curEvt()
    , adc64_data()
    , wf_params()
{
    // Is a directory?
    TSystemDirectory dir("", data_loc);
    TList* files = dir.GetListOfFiles();
    if (files)
    {
        if (!data_loc.EndsWith("/")) data_loc += "/";

        // Has at least one *.root file?
        for (int f_idx=0; f_idx < files->GetEntries(); ++f_idx)
        {
            TString fname = data_loc + files->At(f_idx)->GetName();
            if (gROOT->IsRootFile(fname))
            {
                init(fname);
                return;
            }
        }

        printf("ERROR!!! Cann't find at least one .root file in dir\n");
        return;
    }

    // Is a correct file spec?
    if (gROOT->IsRootFile(data_loc))
    {
        init(data_loc);
        return;
    }

    printf("ERROR!!! File not found or not a root file\n");
}

void Adc64Rdr::init(TString fname)
{
    printf("init with file: %s\n", fname.Data());

    adc64_data = (TTree*) TFile::Open(fname)->Get("adc64_data");
    if (!adc64_data)
    {
        printf("ERROR!!! root file hasn't tree with name 'adc64_data' inside\n");
        return;
    }

    int ch;
    for (ch=0; ch < ARM_SIZE-1; ++ch)
    {
        connectBranch(Form("channel_%d", ch), lArm + ch);
        lArm[ch].type = NaI_FRAME;
    }
    for (; ch < (ARM_SIZE-1)*2; ++ch)
    {
        connectBranch(Form("channel_%d", ch), rArm + ch - ARM_SIZE + 1);
        rArm[ch - ARM_SIZE + 1].type = NaI_FRAME;
    }
    connectBranch(Form("channel_%d", ch++), lArm + ARM_SIZE - 1);
    lArm[ARM_SIZE - 1].type = SCAT_FRAME;
    connectBranch(Form("channel_%d", ch), rArm + ARM_SIZE - 1);
    rArm[ARM_SIZE - 1].type = SCAT_FRAME;

    loadCurEntry();
    fillWfReprs();

    // init histograms in each WfParams obj
    for (int ch=0; ch < ARM_SIZE; ++ch)
    {
        lArmParams[ch].initHists("L", ch + 1, lArm[ch].type);
        rArmParams[ch].initHists("R", ch + 1, rArm[ch].type);
    }
}

void Adc64Rdr::nextEvt()
{
    if (!adc64_data)
    {
        printf("Adc64Rdr doesn't initialized properly!\n");
        return;
    }

    Long64_t full_size = adc64_data->GetEntries();
    if (++curEvt >= full_size) curEvt -= full_size;
    loadCurEntry();
    fillWfReprs();
}

void Adc64Rdr::prevEvt()
{
    if (!adc64_data)
    {
        printf("Adc64Rdr doesn't initialized properly!\n");
        return;
    }

    Long64_t full_size = adc64_data->GetEntries();
    if (--curEvt < 0) curEvt += full_size;
    loadCurEntry();
    fillWfReprs();
}

void Adc64Rdr::connectBranch(const char* br_name, Frame* frm_ptr)
{
    printf("connecting %s branch...\n", br_name);
    adc64_data->SetBranchAddress(br_name, frm_ptr);
}

void Adc64Rdr::loadCurEntry()
{
    printf("loading evt %lld...\n", curEvt);
    adc64_data->GetEntry(curEvt);
}

void Adc64Rdr::fillWfRepr(Frame &frm)
{
    frm.wf_repr.Set(frm.wf_size);
    for (int s=0; s < frm.wf_size; ++s) frm.wf_repr.SetPoint(s, s, frm.wf[s]);
}

void Adc64Rdr::fillWfReprs()
{
    int mult_left=0, mult_right=0;

    printf("filling wf representations...\n");
    for (int ch=0; ch < ARM_SIZE; ++ch)
    {
        fillWfRepr(lArm[ch]);
        fillWfRepr(rArm[ch]);

        if (lArm[ch].wf_size) ++mult_left;
        if (rArm[ch].wf_size) ++mult_right;
    }
    printf("Multiplicity: (%d, %d)\n", mult_left, mult_right);
}

void Adc64Rdr::drawLeftAsset(AssetType t, int ch)
{
    switch (t)
    {
        case WF: lArm[ch].wf_repr.Draw(); break;
        case AMP: lArmParams[ch].hAmp.Draw(); break;
        case BEG: lArmParams[ch].hBeg.Draw(); break;
    }
}

void Adc64Rdr::drawRightAsset(AssetType t, int ch)
{
    switch (t)
    {
        case WF: rArm[ch].wf_repr.Draw(); break;
        case AMP: rArmParams[ch].hAmp.Draw(); break;
        case BEG: rArmParams[ch].hBeg.Draw(); break;
    }
}

void Adc64Rdr::createParamsTree()
{
    Info("createParamsTree", "generating tree..");

    // recreate temporary root file
    TString dir_name = gSystem->DirName(adc64_data->GetDirectory()->GetName());
    new TFile(dir_name + "/tmp.root", "recreate");

    // create and setup params tree
    wf_params = new TTree("wf_params", "Parameters of collected frames");
    int ch;
    for (ch=0; ch < ARM_SIZE-1; ++ch)
    {
        wf_params->Branch(Form("channel_%d", ch), lArmParams + ch, WF_PARAMS_SPEC);
    }
    for (; ch < 2 * (ARM_SIZE-1); ++ch)
    {
        wf_params->Branch(Form("channel_%d", ch), rArmParams + ch - ARM_SIZE + 1, WF_PARAMS_SPEC);
    }
    wf_params->Branch(Form("channel_%d", ch++), lArmParams + ARM_SIZE - 1, WF_PARAMS_SPEC);
    wf_params->Branch(Form("channel_%d", ch), rArmParams + ARM_SIZE - 1, WF_PARAMS_SPEC);

    Info("createParamsTree", "events in adc64_data: %lld", adc64_data->GetEntries());
    
    // fill tree and histograms, save tree
    int NaI_mults[2];
    WfParams* scatParams[] = { &lArmParams[ARM_SIZE-1], &rArmParams[ARM_SIZE-1] };
    for (Long64_t evt=0; evt < adc64_data->GetEntries(); ++evt)
    {
        adc64_data->GetEntry(evt);

        for (int ch=0; ch < ARM_SIZE; ++ch)
        {
            calcParams(lArm[ch], lArmParams[ch]);
            calcParams(rArm[ch], rArmParams[ch]);
        }
  
        NaI_mults[0] = NaI_mults[1] = 0;
        for (int ch=0; ch < ARM_SIZE-1; ++ch)
        {
            if ( lArmParams[ch].beg > -1 ) ++NaI_mults[0];
            if ( rArmParams[ch].beg > -1 ) ++NaI_mults[1];
        }
        for (int i=0; i < 2; ++i)
        {
            if ( scatParams[i]->beg > -1 && NaI_mults[i] )
            {
                scatParams[i]->hAmp.Fill(scatParams[i]->amp);
                scatParams[i]->hBeg.Fill(scatParams[i]->beg);
            }
        }

        wf_params->Fill();
    }
    wf_params->Write();

    Info("createParamsTree", "tree generated");
}

void Adc64Rdr::fitLeftAsset(AssetType t, int ch)
{
    switch (t)
    {
        case AMP:
            fitHist(&lArmParams[ch].hAmp, REDRAW | PRINT);
        break;
        case BEG:
            fitHist(&lArmParams[ch].hBeg, REDRAW | PRINT);
    }
}

void Adc64Rdr::fitRightAsset(AssetType t, int ch)
{
    switch (t)
    {
        case AMP:
            fitHist(&rArmParams[ch].hAmp, REDRAW | PRINT);
        break;
        case BEG:
            fitHist(&rArmParams[ch].hBeg, REDRAW | PRINT);
    }
}

void Adc64Rdr::saveAmpHists()
{
    TFile* f;
    TString dir_name = gSystem->DirName(adc64_data->GetDirectory()->GetName());
    
    f = new TFile(dir_name + "/lArm_amps.root", "recreate");
    for (int ch=0; ch<ARM_SIZE; ++ch) lArmParams[ch].hAmp.Write();
    delete f;

    f = new TFile(dir_name + "/rArm_amps.root", "recreate");
    for (int ch=0; ch<ARM_SIZE; ++ch) rArmParams[ch].hAmp.Write();
    delete f;
}