#include "Viewer.h"
#include "Adc64Rdr.h"
#include "ArmView.h"
#include <TGLayout.h>
#include <TGClient.h>
#include <TGButton.h>
#include <TGTab.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGTableLayout.h>

const int HIST_CANV_W = 400;
const int HIST_CANV_H = 300;

TRootEmbeddedCanvas* getECanv(TList* cans, int idx)
{
    return (TRootEmbeddedCanvas*) ((TGFrameElement*) cans->At(idx))->fFrame;
}


Viewer::Viewer(const char* data_loc)
    : TGMainFrame(gClient->GetRoot())
    , rdr( new Adc64Rdr(data_loc) )
{
    TGButton* btn;
    TGTab* tab = new TGTab(this);
    AddFrame(tab, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    
    // wf monitor part //////////////////////////////////////
    tab1 = new TGHorizontalFrame(tab);
    tab->AddTab("Wf monitor", tab1);

    lView = new ArmView(ARM_SIZE, tab1);
    lView->SetTitle("LeftArm");
    tab1->AddFrame(lView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

    rView = new ArmView(ARM_SIZE, tab1);
    rView->SetTitle("RightArm");
    tab1->AddFrame(rView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

    for (int n=0; n < ARM_SIZE; ++n)
    {
        lView->cd(n);
        rdr->drawLeftAsset(WF, n);

        rView->cd(n);
        rdr->drawRightAsset(WF, n);
    }

    TGCompositeFrame* sideFrm = new TGVerticalFrame(tab1);
    tab1->AddFrame(sideFrm, new TGLayoutHints(kLHintsTop | kLHintsRight));
    
    btn = new TGTextButton(sideFrm, "PREV EVT");
    sideFrm->AddFrame(btn);
    btn->Connect("Clicked()", "Viewer", this, "prevEvt()");
    
    btn = new TGTextButton(sideFrm, "NEXT EVT");
    sideFrm->AddFrame(btn);
    btn->Connect("Clicked()", "Viewer", this, "nextEvt()");
    /////////////////////////////////////////////////////////

    // parametrization part /////////////////////////////////
    tab2 = new TGHorizontalFrame(tab);
    tab->AddTab("Parametrization", tab2);

    TGCanvas* hist_view = new TGCanvas(tab2);
    tab2->AddFrame(hist_view, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    histCont = new TGCompositeFrame(hist_view->GetViewPort());
    histCont->SetLayoutManager( new TGTableLayout(histCont, ARM_SIZE*2, 2) );
    hist_view->SetContainer(histCont);

    TRootEmbeddedCanvas* can;
    for (int ch=0; ch < ARM_SIZE; ++ch)
    {
        can = new TRootEmbeddedCanvas("", histCont, HIST_CANV_W, HIST_CANV_H);
        histCont->AddFrame(can, new TGTableLayoutHints(0, 1, ch, ch + 1));
        can->GetCanvas()->cd();
        rdr->drawLeftAsset(AMP, ch);

        can = new TRootEmbeddedCanvas("", histCont, HIST_CANV_W, HIST_CANV_H);
        histCont->AddFrame(can, new TGTableLayoutHints(1, 2, ch, ch + 1));
        can->GetCanvas()->cd();
        rdr->drawLeftAsset(BEG, ch);
    }
    for (int ch=0; ch < ARM_SIZE; ++ch)
    {
        can = new TRootEmbeddedCanvas("", histCont, HIST_CANV_W, HIST_CANV_H);
        histCont->AddFrame(can, new TGTableLayoutHints(0, 1, ch + ARM_SIZE, ch + ARM_SIZE + 1));
        can->GetCanvas()->cd();
        rdr->drawRightAsset(AMP, ch);

        can = new TRootEmbeddedCanvas("", histCont, HIST_CANV_W, HIST_CANV_H);
        histCont->AddFrame(can, new TGTableLayoutHints(1, 2, ch + ARM_SIZE, ch + ARM_SIZE + 1));
        can->GetCanvas()->cd();
        rdr->drawRightAsset(BEG, ch);
    }

    TGCompositeFrame* sideFrm2 = new TGVerticalFrame(tab2);
    tab2->AddFrame(sideFrm2, new TGLayoutHints(kLHintsTop | kLHintsRight));
    
    btn = new TGTextButton(sideFrm2, "EXTRACT PARAMS");
    sideFrm2->AddFrame(btn);
    btn->Connect("Clicked()", "Viewer", this, "extractParams()");
    btn = new TGTextButton(sideFrm2, "SAVE AMPS");
    sideFrm2->AddFrame(btn);
    btn->Connect("Clicked()", "Adc64Rdr", rdr, "saveAmpHists()");
    /////////////////////////////////////////////////////////
    
    
    MapSubwindows();
    Layout();
    MapWindow();
    Resize(900, 600);
}

Viewer::~Viewer()
{
    delete rdr;
}

void Viewer::prevEvt()
{
    rdr->prevEvt();
    lView->update();
    rView->update();
}

void Viewer::nextEvt()
{
    rdr->nextEvt();
    lView->update();
    rView->update();
}

void Viewer::extractParams()
{
    rdr->createParamsTree();

    // update canvases with filled and fitted hists
    TCanvas* c;
    for (int ch=0; ch < ARM_SIZE; ++ch)
    {
        c = getECanv( histCont->GetList(), ch*2 )->GetCanvas();
        c->cd();
        rdr->fitLeftAsset(AMP, ch);
        c->Modified();
        c->Update();

        c = getECanv( histCont->GetList(), ch*2 + 1 )->GetCanvas();
        c->cd();
        // rdr->fitLeftAsset(BEG, ch);
        c->Modified();
        c->Update();

        c = getECanv( histCont->GetList(), (ch+ARM_SIZE)*2 )->GetCanvas();
        c->cd();
        // rdr->fitRightAsset(AMP, ch);
        c->Modified();
        c->Update();

        c = getECanv( histCont->GetList(), (ch+ARM_SIZE)*2 + 1 )->GetCanvas();
        c->cd();
        // rdr->fitRightAsset(BEG, ch);
        c->Modified();
        c->Update();
    }
}