#include "ArmLayout.h"
#include <TMath.h>
#include <TGFrame.h>
#include <vector>
#include <numeric>

using namespace std;

const double CELL_W = 0.9;
const double CELL_H = 0.6;
const int N_SUB_ANGLES = 10;

const char RIGHT = 0b01;
const char DOWN = 0b10;

#define CALC_X {    x = TMath::Sin(phi);                    \
                    desc_x = x - locs.back().first;         \
                    over_x = CELL_W - TMath::Abs(desc_x);   }

#define CALC_Y {    y = -TMath::Cos(phi);                   \
                    desc_y = y - locs.back().second;        \
                    over_y = CELL_H - TMath::Abs(desc_y);   }

char evalAngle(double angle)
{
    int n = int(angle / TMath::Pi() / 2);
    if (n) angle -= n * TMath::Pi() * 2;

    if (angle < TMath::Pi() / 2) return RIGHT;
    if (angle < TMath::Pi()) return RIGHT | DOWN;
    if (angle < TMath::Pi() * 1.5) return DOWN;
    return 0;
}

ArmLayout::ArmLayout(TGCompositeFrame* main)
{
    fMain = main;
}

void ArmLayout::Layout()
{
    TList* cells = fMain->GetList();
    vector<pair<double, double>> locs;
    double  phi, prev_phi, delta_phi, 
        x, desc_x, over_x,
        y, desc_y, over_y,
        min_over;
    prev_phi = 0;
    delta_phi = TMath::Pi() * 2 / (cells->GetEntries() - 1);
    
    // layout first NaI
    locs.push_back( make_pair(.0, -1.0) );
    prev_phi = 0;

    // layout rest NaI's
    for (int i = 1; i < cells->GetEntries() - 1; ++i)
    {
        phi = i * delta_phi;
        if (phi < prev_phi) phi = prev_phi;
        
        CALC_X
        CALC_Y

        while ( over_x > 0 && over_y > 0 )
        {
            if ( over_x < over_y )
            {
                if ( (desc_x > 0) == (evalAngle(phi) & RIGHT) )
                {
                    x += TMath::Sign(over_x, desc_x);
                    break;
                }
            }
            else
            {
                if ( (desc_y > 0) == (evalAngle(phi) & DOWN) )
                {
                    y += TMath::Sign(over_y, desc_y);
                    break;
                }
            }

            phi += delta_phi / N_SUB_ANGLES;
            CALC_X
            CALC_Y
        }

        locs.push_back( make_pair(x, y) );
        prev_phi = phi;
    }

    // layout scatterer
    locs.push_back( make_pair(.0, .0) );

    // calc rect
    double top, right, bottom, left;
    left = top = numeric_limits<double>::max();
    right = bottom = numeric_limits<double>::min();
    for (const auto &loc : locs)
    {
        if (loc.first < left) left = loc.first;
        if (loc.first > right) right = loc.first;
        if (loc.second < top) top = loc.second;
        if (loc.second > bottom) bottom = loc.second;
    }
    left -= CELL_W / 2;
    right += CELL_W / 2;
    top -= CELL_H / 2;
    bottom += CELL_H / 2;

    // calc scale
    int w, h;
    double scale_x, scale_y;
    w = fMain->GetWidth() - fMain->GetBorderWidth() * 2;
    h = fMain->GetHeight() - fMain->GetBorderWidth() * 2;
    scale_x = w / (right - left);
    scale_y = h / (bottom - top);

    // scale and shift
    for (int i=0; i < locs.size(); ++i)
    {
        TGFrame* frm = ((TGFrameElement*) cells->At(i))->fFrame;
        frm->MoveResize(    locs[i].first * scale_x + fMain->GetWidth() / 2,
                            locs[i].second * scale_y + fMain->GetHeight() / 2,
                            CELL_W * scale_x,
                            CELL_H * scale_y    );
    }
}

TGDimension ArmLayout::GetDefaultSize() const
{
    return TGDimension(10, 10);
}
